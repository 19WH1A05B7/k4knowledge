
import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
@Component({
  selector: 'app-display',
  templateUrl: './display.component.html',
  styleUrls: ['./display.component.css']
})
export class DisplayComponent implements OnInit {

  userdetails: any;
  constructor(private service: UserService) { 
    this.userdetails = {name: '', email:'', password:''};
  }

  ngOnInit(): void {
    this.service.showAllUsers().subscribe((result: any)=>{
      this.userdetails = result;
    })
  }

}