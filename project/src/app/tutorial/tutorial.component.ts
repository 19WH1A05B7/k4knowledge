import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TutorialService } from '../tutorial.service';
import {Tutorials} from'../shared/models/Tutorials';
import { RatingModule } from 'ng-starrating';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.component.html',
  styleUrls: ['./tutorial.component.css']
})
export class TutorialComponent implements OnInit {
tutorials !: Tutorials;
Tutorials : Tutorials [] =[];
  constructor(private activatedRoute:ActivatedRoute, private TutorialService:TutorialService,
    private route:Router) {
      activatedRoute.params.subscribe((params)=>{
        if(params['id'])
          this.tutorials = TutorialService.getTutorialById(params['id']);
      })
     }

  ngOnInit(): void {

  }
  }
