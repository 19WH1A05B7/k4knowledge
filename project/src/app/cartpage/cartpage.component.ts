import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { CourseService } from '../course.service';
import { Cart } from '../shared/models/Cart';
import { CartItem } from '../shared/models/CartItem';
@Component({
  selector: 'app-cartpage',
  templateUrl: './cartpage.component.html',
  styleUrls: ['./cartpage.component.css']
})
export class CartPageComponent implements OnInit {

  cart! :Cart;
  constructor(private CartService: CartService , private CourseService:CourseService) {
    let courses = CourseService.getAll();
    CartService.addToCart(courses[1]);
    CartService.addToCart(courses[3]);
    CartService.addToCart(courses[5]);
    this.setCart();
  }
  ngOnInit(): void {
  }
  setCart() {
    this.cart = this.CartService.getCart();
  }
  removeFromCart(cartItem: CartItem) {
    this.CartService.removeFromCart(cartItem.course.id);
    this.setCart(); //instance load data 
  }
  changeQuantity(CartItem: CartItem,quantityInstring:string){
    const quantity = parseInt(quantityInstring);
    this.CartService.changeQuantity(CartItem.course.id, quantity);
    this.setCart();
  }

}