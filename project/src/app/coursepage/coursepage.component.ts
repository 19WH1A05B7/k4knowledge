import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { CourseService } from '../course.service';
import { CartService } from '../cart.service';
import { BookmarkService } from '../bookmark.service';
import {Courses} from'../shared/models/Courses';


@Component({
  selector: 'app-coursepage',
  templateUrl: './coursepage.component.html',
  styleUrls: ['./coursepage.component.css']
})
export class CoursePageComponent implements OnInit {
courses !: Courses;
Courses : Courses [] =[];
  constructor(private activatedRoute:ActivatedRoute, private courseService:CourseService , private cartService:CartService, private BookmarkService:BookmarkService ,
    private route:Router) {
      activatedRoute.params.subscribe((params)=>{
        if(params['id'])
          this.courses = courseService.getCourseById(params['id']);
      })
     }

  ngOnInit(): void {

  }
  addToCart(){
    this.cartService.addToCart(this.courses);
    this.route.navigateByUrl('/cartpage');  
  }
  addtoBookmark(){
    this.BookmarkService.addtoBookmark(this.courses);
    this.route.navigateByUrl('/bookmark');  
  }

}
