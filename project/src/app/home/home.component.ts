import { Component, OnInit } from '@angular/core';
import { CourseService } from '../course.service';
import { Courses } from '../shared/models/Courses';
import {ActivatedRoute} from '@angular/router';
import {Router} from '@angular/router';
import { CartService } from '../cart.service';
import { BookmarkService } from '../bookmark.service';
import { TutorialService } from '../tutorial.service';
import { Tutorials } from '../shared/models/Tutorials';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  tutorials !: Tutorials;
  Tutorials:Tutorials[] = [];
  courses !: Courses;
  Courses:Courses[] = [];

  constructor(private cs:CourseService,private ts:TutorialService, private router:ActivatedRoute , private route:Router,private cartService:CartService , private bookmarkService:BookmarkService) {

  }

  ngOnInit(): void {
    
    this.router.params.subscribe(params =>{
        if(params['searchItem'])
        this.Courses = this.cs.getAll().filter(course => course.name.toLowerCase().includes(params['searchItem'].toLowerCase()));
        else if(params.tag)
        this.Courses = this.cs.getAllCourseByTag(params['tag']);
        else
        this.Courses=this.cs.getAll();
        this.Tutorials=this.ts.getAll();
      })
      
  }
}
