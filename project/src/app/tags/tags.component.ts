import { Component, Input, OnInit } from '@angular/core';
import {CourseService} from '../course.service'
import {Tag} from '../shared/models/Tag';

@Component({
  selector: 'app-tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {
  @Input()
  coursePageTags?:string[];

  tags?: Tag[] =[];


  constructor(private cs:CourseService) { }

  ngOnInit(): void {
    if(!this.coursePageTags)
    this.tags = this.cs.getAllTag();
  }

}
