import { Component, OnInit } from '@angular/core';
import { BookmarkService } from '../bookmark.service';
import { CourseService} from '../course.service';
import { Bookmark } from '../shared/models/Bookmark';
import { BookmarkItem } from '../shared/models/BookmarkItem';
@Component({
  selector: 'app-bookmark',
  templateUrl: './bookmark.component.html',
  styleUrls: ['./bookmark.component.css']
})
export class BookmarkComponent implements OnInit {

  bookmark! :Bookmark;
  constructor(private BookmarkService: BookmarkService , private CourseService:CourseService) {
    let courses = CourseService.getAll();
    // BookmarkService.addtoBookmark(courses[1]);
    // BookmarkService.addtoBookmark(courses[3]);
    BookmarkService.addtoBookmark(courses[5]);
    this.SetBookmark();
  }
  ngOnInit(): void {
  }
  SetBookmark() {
    this.bookmark = this.BookmarkService.getBookmark();
  }
  removeFromBookmark(BookmarkItem: BookmarkItem) {
    this.BookmarkService.removeFromBookmark(BookmarkItem.course.id);
    this.SetBookmark(); //instance load data 
  }
}
