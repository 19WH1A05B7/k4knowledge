﻿let express = require("express");
let bodyparser = require("body-parser");
let cors = require("cors");
let app =  express();
app.use(bodyparser.json());
app.use(bodyparser.urlencoded({extended:false}));
app.use(cors());
app.use("/login",require("./login"));
app.use("/register",require("./register"));
// app.use("/email",require("./index"));

app.use("/fetch",require("./fetch"));
app.listen(3000);
console.log("server listening the port no.3000");